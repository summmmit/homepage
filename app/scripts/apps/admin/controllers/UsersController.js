app.controller('UsersController', ['$scope', '$timeout', function($scope, $timeout) {
  
  $scope.userData = [
      {FirstName: 'Laurent', LastName: 'Renard', UserName: 'Renard', RegistredAt: new Date(), email: 'whatever@gmail.com', Status: 1 },
      {FirstName: 'Blandine', LastName: 'Faivre', UserName: 'ShmiRenard', RegistredAt: new Date('1987-04-25'), email: 'oufblandou@gmail.com', Status: 2 },
      {FirstName: 'Francoise', LastName: 'Frere', UserName: 'summmmit', RegistredAt: new Date('1955-08-27'), email: 'raymondef@gmail.com', Status: 3 }
    ];
    
  $scope.Statuses = {
    1: 'Active', 2: 'Suspended', 3: 'Disabled'
  };
  
  $scope.UserSessions = [
      { Ip: '10.10.11.2', UserAgent: 'Mozzila', LastLoginedAt: new Date('1987-04-25')},
      { Ip: '10.10.11.21', UserAgent: 'Chrome', LastLoginedAt: new Date('1982-06-11')},
      { Ip: '10.10.11.26', UserAgent: 'Linux', LastLoginedAt: new Date()},
  ];
  
  $scope.rowCollectionBasic = [
      {firstName: 'Laurent', lastName: 'Renard', birthDate: new Date('1987-05-21'), balance: 102, email: 'whatever@gmail.com'},
      {firstName: 'Blandine', lastName: 'Faivre', birthDate: new Date('1987-04-25'), balance: -2323.22, email: 'oufblandou@gmail.com'},
      {firstName: 'Francoise', lastName: 'Frere', birthDate: new Date('1955-08-27'), balance: 42343, email: 'raymondef@gmail.com'}
  ];

  $scope.removeRow = function(row) {
      var index = $scope.rowCollectionBasic.indexOf(row);
      if (index !== -1) {
          $scope.rowCollectionBasic.splice(index, 1);
      }
  };

  $scope.predicates = ['firstName', 'lastName', 'birthDate', 'balance', 'email'];
  $scope.selectedPredicate = $scope.predicates[0];

  var firstnames = ['Laurent', 'Blandine', 'Olivier', 'Max'];
  var lastnames = ['Renard', 'Faivre', 'Frere', 'Eponge'];
  var dates = ['1987-05-21', '1987-04-25', '1955-08-27', '1966-06-06'];
  var id = 1;

  function generateRandomItem(id) {

      var firstname = firstnames[Math.floor(Math.random() * 3)];
      var lastname = lastnames[Math.floor(Math.random() * 3)];
      var birthdate = dates[Math.floor(Math.random() * 3)];
      var balance = Math.floor(Math.random() * 2000);

      return {
          id: id,
          firstName: firstname,
          lastName: lastname,
          birthDate: new Date(birthdate),
          balance: balance
      }
  }

  $scope.rowCollection = [];

  for (id; id < 5; id++) {
      $scope.rowCollection.push(generateRandomItem(id));
  }

  //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
  $scope.displayedCollection = [].concat($scope.rowCollection);

  //add to the real data holder
  $scope.addRandomItem = function addRandomItem() {
      $scope.rowCollection.push(generateRandomItem(id));
      id++;
  };

  //remove to the real data holder
  $scope.removeItem = function(row) {
      var index = $scope.rowCollection.indexOf(row);
      if (index !== -1) {
          $scope.rowCollection.splice(index, 1);
      }
  }

  //  pagination
  $scope.itemsByPage=5;

  $scope.rowCollectionPage = [];
  for (var j = 0; j < 10; j++) {
    $scope.rowCollectionPage.push(generateRandomItem(j));
  }

  // pip
  var promise = null;
  $scope.isLoading = false;
  $scope.rowCollectionPip = [];
  $scope.getPage = function() {
    $scope.rowCollectionPip=[];
    for (var j = 0; j < 20; j++) {
      $scope.rowCollectionPip.push(generateRandomItem(j));
    }
  }

  $scope.callServer = function getData(tableState) {
      //here you could create a query string from tableState
      //fake ajax call
      $scope.isLoading = true;

      $timeout(function () {
          $scope.getPage();
          $scope.isLoading = false;
      }, 2000);
  };

 $scope.getPage();

}]);


app.filter('UserStatus', function() {
    return function(status) {
      
      if (status == 1){
        return 'Active';
      } else if (status == 2){
        return 'Suspended';
      } else if (status == 3){
        return 'Disabled';
      }
    }
  });
