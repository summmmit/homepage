app.controller('DomainsController', ['$scope', function($scope) {
    $scope.oneAtATime = true;

    $scope.DomainsList = [
      {
        Name: 'Google Drive',
        Description: 'File Drive',
        Url: '',
        Ip: '10.10.10.10',
        Port: '8080',
        Type: 1
      },
      {
        Name: 'Google File',
        Description: 'File Drive',
        Url: 'http://www.google.com/drive',
        Ip: '10.10.10.10',
        Port: '8080',
        Type: 2
      },
      {
        Name: 'Facebook',
        Description: 'File Drive',
        Url: 'http://www.google.com/drive',
        Ip: '10.10.10.10',
        Port: '8080',
        Type: 2
      },
      {
        Name: 'Twitter',
        Description: 'File Drive',
        Url: 'http://www.google.com/drive',
        Ip: '10.10.10.10',
        Port: '8080',
        Type: 1
      },
    ];
    
    $scope.addNewDomain = function() {
      
      console.log($scope.DomainsList);
      
      var domain = 
      {
        Name: 'Google Drive',
        Description: 'File Drive',
        Url: '',
        Ip: '10.10.10.10',
        Port: '8080',
        Type: 1
      }
      
      $scope.DomainsList.push(domain);
      
      console.log($scope.DomainsList);
      
    }

    $scope.items = ['Item 1', 'Item 2', 'Item 3'];

    $scope.addItem = function() {
      var newItemNo = $scope.items.length + 1;
      $scope.items.push('Item ' + newItemNo);
    };

    $scope.status = {
      isFirstOpen: true,
      isFirstDisabled: false
    };
  }]
);  

app.filter('DomainLinkFilter', function(){
  return function(domain, Ip, Port){
    if (domain) {
      return domain;
    } else {
      return 'http://' + Ip + ':' + Port;
    }
  }
});

app.filter('DomainTypeFilter', function(){
  return function(domainType){
    if (domainType == 1){
      return 'Development';
    } else if (domainType == 2){
      return 'Media';
    } else if (domainType == 3){
      return 'Other'
    }
  }
});